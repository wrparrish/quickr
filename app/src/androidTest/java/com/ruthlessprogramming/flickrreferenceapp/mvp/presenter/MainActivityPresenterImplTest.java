package com.ruthlessprogramming.flickrreferenceapp.mvp.presenter;

import com.ruthlessprogramming.flickrreferenceapp.mvp.model.Photo;
import com.ruthlessprogramming.flickrreferenceapp.mvp.model.PhotoResponse;
import com.ruthlessprogramming.flickrreferenceapp.mvp.model.Photos;
import com.ruthlessprogramming.flickrreferenceapp.mvp.view.MainActivityView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by williamparrish on 6/25/16.
 */

public class MainActivityPresenterImplTest {
    private MainActivityPresenterImpl presenter;
    private MainActivityView view;
    private PhotoResponse response;


    @Before
    public void setUp() throws Exception {
        presenter = new MainActivityPresenterImpl();
        view = Mockito.mock(MainActivityView.class);
        presenter.attachView(view);

        response = new PhotoResponse();
        Photos photos = new Photos();
        ArrayList<Photo> blankPhotos = new ArrayList<>();
        blankPhotos.add(new Photo());
        blankPhotos.add(new Photo());
        photos.setPhoto(blankPhotos);
        response.setPhotos(photos);
    }

    @Test
    public void retrieve_photos_does_show_loading() throws Exception {
        presenter.retrievePhotos("Test text");
        verify(view, times(1)).showLoading();
    }

    @Test
    public void good_response_shows_photos() throws Exception {
        presenter.processResponseData(response);
        verify(view, times(1)).showContent(response.getPhotos().getPhoto());
    }

    @Test
    public void bad_response_shows_error() throws Exception {
        response.getPhotos().setPhoto(null);
        presenter.processResponseData(response);
        verify(view, times(1)).showError("Unable to retrieve photos related to [null]");
    }


    @Test
    public void loadStarterPhotos() throws Exception {
        presenter.loadStarterPhotos();
        verify(view, times(1)).showLoading();
        presenter.processResponseData(response);
        verify(view).showContent(response.getPhotos().getPhoto());

    }

}