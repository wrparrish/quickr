package com.ruthlessprogramming.flickrreferenceapp;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.ruthlessprogramming.flickrreferenceapp.dagger.AndroidModule;
import com.ruthlessprogramming.flickrreferenceapp.dagger.DaggerFlickrComponent;
import com.ruthlessprogramming.flickrreferenceapp.dagger.FlickrComponent;
import com.ruthlessprogramming.flickrreferenceapp.dagger.Injector;
import com.ruthlessprogramming.flickrreferenceapp.dagger.ManagerModule;
import com.ruthlessprogramming.flickrreferenceapp.dagger.NetModule;
import com.ruthlessprogramming.flickrreferenceapp.management.NetworkManager;
import com.ruthlessprogramming.flickrreferenceapp.management.OnConnectionChangedListener;

import javax.inject.Inject;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */

public class App extends Application {
    public static Injector injector;

    @Inject
    NetworkManager networkManager;


    @Override
    public void onCreate() {
        super.onCreate();
        initStetho();
        initDagger();
    }

    private void initDagger() {
        FlickrComponent flickrComponent = createComponent();
        injector = Injector.get(flickrComponent);

        injector.inject(this);
    }

    private FlickrComponent createComponent() {
        return DaggerFlickrComponent.builder()
                .androidModule(new AndroidModule(this))
                .netModule(new NetModule())
                .managerModule(new ManagerModule())
                .build();
    }

    private void initStetho() {
        final Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);

        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );

        // Enable command line interface
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this)
        );

        // Use the InitializerBuilder to generate an Initializer
        final Stetho.Initializer initializer = initializerBuilder.build();

        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer);


    }

    public void addConnectionListener(OnConnectionChangedListener listener) {
        networkManager.addStatusListener(listener);
    }

    public void removeConnectionListener(OnConnectionChangedListener listener) {
        networkManager.removeStatusListener(listener);
    }
}
