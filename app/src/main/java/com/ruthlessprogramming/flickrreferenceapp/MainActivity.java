package com.ruthlessprogramming.flickrreferenceapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.pwittchen.reactivenetwork.library.ConnectivityStatus;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;
import com.ruthlessprogramming.flickrreferenceapp.management.OnConnectionChangedListener;
import com.ruthlessprogramming.flickrreferenceapp.mvp.model.Photo;
import com.ruthlessprogramming.flickrreferenceapp.mvp.presenter.MainActivityPresenter;
import com.ruthlessprogramming.flickrreferenceapp.mvp.presenter.MainActivityPresenterImpl;
import com.ruthlessprogramming.flickrreferenceapp.mvp.view.MainActivityView;
import com.ruthlessprogramming.flickrreferenceapp.mvp.view.rec_view.FlickrPhotoAdapter;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;

public class MainActivity extends MvpActivity<MainActivityView, MainActivityPresenter> implements MainActivityView, OnConnectionChangedListener {
    private static final String TAG = "MainActivity";
    @Bind(R.id.offline_view)
    LinearLayout offlineView;
    @Bind(R.id.loadingView)
    ProgressBar loadingView;
    @Bind(R.id.rv_photo_list)
    RecyclerView recyclerView;
    @Bind(R.id.errorView)
    TextView errorView;

    private App app;
    private ConnectivityStatus currentNetStatus;
    private FlickrPhotoAdapter adapter;
    private SearchView searchView;
    private Subscription querySubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        App.injector.inject(this);
        app = (App) getApplication();
        setupRecyclerView();
        getStarterPhotos();
    }

    @Override
    protected void onResume() {
        super.onResume();
        app.addConnectionListener(this);
        subscribeToQueryChanges();
    }

    @Override
    protected void onPause() {
        super.onPause();
        app.removeConnectionListener(this);
        unsubscribeFromQueryUpdates();
    }


    private void setupRecyclerView() {
        final int orientation = LinearLayoutManager.VERTICAL;
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, orientation, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new FlickrPhotoAdapter(this);
        recyclerView.setAdapter(adapter);
    }

    private void unsubscribeFromQueryUpdates() {
        if (querySubscription != null && !querySubscription.isUnsubscribed()) {
            querySubscription.unsubscribe();
        }

    }

    private void subscribeToQueryChanges() {
        if (searchView == null) {
            return;
        }
        querySubscription = RxSearchView.queryTextChanges(searchView)
                .debounce(900, TimeUnit.MILLISECONDS)
                .filter(new Func1<CharSequence, Boolean>() {
                    @Override
                    public Boolean call(CharSequence charSequence) {
                        return (charSequence != null && charSequence.length() > 0);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CharSequence>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(CharSequence charSequence) {
                        Log.d(TAG, "onNext() called with: charSequence = [" + charSequence.toString() + "]");
                        presenter.retrievePhotos(charSequence.toString());
                        searchView.clearFocus();

                    }
                });

    }

    private void getStarterPhotos() {
        presenter.loadStarterPhotos();
    }

    @NonNull
    @Override
    public MainActivityPresenter createPresenter() {
        return new MainActivityPresenterImpl();
    }


    private boolean isInternetConnected() {
        return (currentNetStatus == ConnectivityStatus.MOBILE_CONNECTED || currentNetStatus == ConnectivityStatus.WIFI_CONNECTED);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        subscribeToQueryChanges();
        return true;
    }


    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading() called");
        if (errorView.isShown()) {
            hideError();
        }

        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showContent(ArrayList<Photo> photoArrayList) {
        Log.d(TAG, "showContent() called with: photoArrayList = [" + photoArrayList + "]");
        hideLoading();
        adapter.setPhotos(photoArrayList);
        adapter.notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(0);
    }


    @Override
    public void showError(String message) {
        Log.e(TAG, "showError() called with: message = [" + message + "]");
        hideLoading();
        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideError() {
        errorView.setVisibility(View.GONE);
    }


    @Override
    public void hideOffline() {
        offlineView.setVisibility(View.GONE);
    }

    @Override
    public void showOffline() {
        offlineView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConnectionChanged(ConnectivityStatus connectivityStatus) {
        currentNetStatus = connectivityStatus;
        if (!isInternetConnected()) {
            showOffline();

        } else {
            hideOffline();
        }
    }

    @Override
    public void onCurrentConnectionStatus(ConnectivityStatus connectivityStatus) {
        currentNetStatus = connectivityStatus;
    }

    @Override
    public String provideName() {
        return TAG;
    }

    @OnClick(R.id.errorView)
    void retry() {
        getStarterPhotos();
    }
}
