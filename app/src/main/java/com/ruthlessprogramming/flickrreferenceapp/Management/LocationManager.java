package com.ruthlessprogramming.flickrreferenceapp.management;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.location.LocationRequest;
import com.ruthlessprogramming.flickrreferenceapp.service.AddressService;
import com.ruthlessprogramming.flickrreferenceapp.service.ServiceResultReceiver;

import java.util.ArrayList;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Billy on 4/2/2016 for SDK.
 * <p/>
 * This class is responsible for being entry point to all LocationRequest activities.
 * It maintains the subscriptions for GPS updates, and distributes those locations to
 * an address service for address lookup. Additionally,  it maintains the subscriptions
 * by monitoring a list of  callback interfaces,  when empty,  the class un-subscribes from
 * its gps updates.  When a listener registers,  it begins the subscription again.
 */
public class LocationManager implements ServiceResultReceiver.Receiver {
    private static final String TAG = "LocationManager";

    private static final int ADDRESS_REQUEST_GPS = 0;

    private final ArrayList<OnLocationChangedListener> listenerList;
    private final Context context;
    private final LocationRequest requestFastInterval;
    private final ReactiveLocationProvider locationProvider;
    private volatile double gpsLat;
    private volatile double gpsLong;
    private Location currentGpsLocation;
    private ServiceResultReceiver receiver;
    private String addressFromGps = "";


    private Subscription locationUpdateSubscription;

    public LocationManager(Context context) {
        Log.d(TAG, "LocationManager: initialized");
        this.context = context;
        listenerList = new ArrayList<>(5);
        locationProvider = new ReactiveLocationProvider(context);
        requestFastInterval = new LocationRequest().create()
                .setInterval(5000)  // interval in milliseconds
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        setupService();
    }

    private void setupService() {
        receiver = new ServiceResultReceiver(new Handler());
        receiver.setReceiver(this);
    }

    public void warmGps() {
        locationProvider.getUpdatedLocation(new LocationRequest().create()
                .setInterval(1000)  // milliseconds
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)) // end request
                .take(3)  // close stream after 3 emissions
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Location>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "warm GPS - onCompleted() called");

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "warm gps - onError: [" + e + "]" );
                    }

                    @Override
                    public void onNext(Location location) {
                        if (location != null) {
                            Log.d(TAG, "onNext() called with: location = [" + location + "]");
                            currentGpsLocation = location;
                            gpsLat = location.getLatitude();
                            gpsLong = location.getLongitude();
                        }

                    }
                });
    }

    /**
     * This method handled adding listeners to our list, and  sending results to them upon  them being added.  If the listener
     * added is the first item added to the list, or is the first item added to a list that had become empty,  it triggers
     * a call to subscribe to location updates.
     *
     * @param listener the screen or presenter interested in location updates, identifiable by TAG
     */
    public void addListener(OnLocationChangedListener listener) {
        Log.d(TAG, "addListener: [" + listener.provideName() + "]");

        // check if listener already registered
        if (containsLocationListener(listener)) {
            return;
        }

        // check if the list is currently empty, and resubscribe to updates if so.
        if (listenerList.size() == 0) {
            subscribeToLocationUpdates();
            listenerList.add(listener);
        } else {
            listenerList.add(listener);
        }

        // give the listener the latest information available
        if (currentGpsLocation != null) {
            listener.onLocationChanged(currentGpsLocation);
        }

    }

    /**
     * This method removes a listener from the list maintained by the location manager. If this causes the list
     * to be empty,  it then makes a follow on call to stop location updates.
     *
     * @param listener the screen or presenter interested in location updates, identifiable by TAG
     */
    public void removeListener(OnLocationChangedListener listener) {
        listenerList.remove(listener);

        if (listenerList.size() == 0) {
            // there are no registered connection listeners
            Log.d(TAG, "[ " + listener.provideName() + "] Last listener was removed, un-subscribing from GPS Updates");
            unsubscribeFromLocationUpdates();
        }

    }

    private void unsubscribeFromLocationUpdates() {
        locationUpdateSubscription.unsubscribe();
    }

    private void subscribeToLocationUpdates() {
        locationUpdateSubscription = locationProvider
                .getUpdatedLocation(requestFastInterval)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Location>() {
                    @Override
                    public void onCompleted() {
                        // infinite stream, this should not get called.
                        // we have to unsubscribe to stop these updates.
                        Log.i(TAG, "subscribeToLocationUpdates.onCompleted()");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "subscribeToLocationUpdates.onError() [" + e + "]");
                    }

                    @Override
                    public void onNext(Location location) {

                        if (location != null && doesPassErrorCheck(location)) {
                            fetchAddressStringForLocation(location);
                            currentGpsLocation = location;
                            gpsLat = location.getLatitude();
                            gpsLong = location.getLongitude();
                            updateListenersWithLocation(location);


                        }

                    }
                });

    }

    private boolean doesPassErrorCheck(Location location) {
        return !(location.getLatitude() == 0 | location.getLongitude() == 0);
    }

    private void updateListenersWithLocation(Location location) {
        for (OnLocationChangedListener listener : listenerList) {
            listener.onLocationChanged(location);
        }
    }


    public Location getCurrentGpsLocation() {
        return currentGpsLocation;
    }

    private boolean containsLocationListener(OnLocationChangedListener listener) {
        boolean isListenerInList = false;

        for (OnLocationChangedListener item : listenerList) {
            if (item.provideName().equals(listener.provideName())) {
                isListenerInList = true;
            }
        }

        return isListenerInList;
    }

    public double getGpsLong() {
        return gpsLong;
    }

    public double getGpsLat() {
        return gpsLat;
    }

    private void fetchAddressStringForLocation(Location location) {
        Intent intent = new Intent(context, AddressService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("receiver", receiver);
        bundle.putDouble("lat", location.getLatitude());
        bundle.putDouble("lng", location.getLongitude());
        intent.putExtras(bundle);
        context.startService(intent);
    }

    @Override
    public void onReceiveResult(final int resultCode, final Bundle resultData) {
        Log.d(TAG, "onReceiveResult() called with: " + "resultCode = [" + resultCode + "], resultData = [" + resultData + "]");

        switch (resultCode) {
            case ADDRESS_REQUEST_GPS:
                addressFromGps = resultData.getString("address", "No address retrieved");
                break;

            default:

        }
    }

    @SuppressWarnings("MissingPermission")
    public Location getCurrentLocationFromAndroidLocationManager() {
        Log.d(TAG, "getCurrentLocationFromAndroidLocationManager() was called");

        android.location.LocationManager locationManager = (android.location.LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setAltitudeRequired(true);

        return locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
    }

    public String getAddressFromGps() {
        return addressFromGps;
    }
}