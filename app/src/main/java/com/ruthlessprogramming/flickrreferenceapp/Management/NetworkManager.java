package com.ruthlessprogramming.flickrreferenceapp.management;


import android.content.Context;
import android.util.Log;

import com.facebook.network.connectionclass.ConnectionClassManager;
import com.facebook.network.connectionclass.ConnectionQuality;
import com.github.pwittchen.reactivenetwork.library.ConnectivityStatus;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;

import java.io.IOException;
import java.util.ArrayList;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * Created by billy on 6/22/2016.
 * for FlickrReferenceApp
 */
public class NetworkManager implements ConnectionClassManager.ConnectionClassStateChangeListener {
    private static final String TAG = "NetworkManager";
    private final ArrayList<ConnectionClassManager.ConnectionClassStateChangeListener> qualityList = new ArrayList<>(4);
    private final ArrayList<OnConnectionChangedListener> statusList = new ArrayList<>(4);
    private ConnectionQuality netQuality;
    private ConnectivityStatus netStatus;
    private Context context;
    private Subscription networkSubscription;


    public NetworkManager(Context context) {
        this.context = context;
        ConnectionClassManager qualityManager = ConnectionClassManager.getInstance();
        qualityManager.register(this);
        subscribeToNetworkChanges();
    }

    public static <T> Observable<T> handleNoConnectionObservable(Observable<T> o) {
        return o.onErrorResumeNext(new Func1<Throwable, Observable<T>>() {
            public Observable<T> call(Throwable err) {
                if (err instanceof IOException) {
                    // network / connectivity related failure
                    return Observable.empty();
                } else {
                    return Observable.error(err);
                }
            }
        });
    }

    public void addStatusListener(OnConnectionChangedListener listener) {
        // check if listener already registered
        if (containsStatusListener(listener)) {
            return;
        }

        // check if the list is currently empty, and resubscribe to updates if so.
        if (statusList.size() == 0) {
            subscribeToNetworkChanges();
            statusList.add(listener);
        } else {
            statusList.add(listener);
        }


        if (netStatus != null) {
            listener.onCurrentConnectionStatus(netStatus);
        }

    }

    public void addQualityListener(ConnectionClassManager.ConnectionClassStateChangeListener listener) {
        //// TODO: 6/29/2016  Add Tag to connection state listener, and check for presence in list before adding
        qualityList.add(listener);

        if (netQuality != null) {
            listener.onBandwidthStateChange(netQuality);
        }

    }


    /**
     * This method removes a listener from the list maintained by the network manager. If this causes the list
     * to be empty,  it then makes a follow on call to stop network updates.
     *
     * @param listener the screen or presenter interested in network updates, identifiable by TAG
     */
    public void removeStatusListener(OnConnectionChangedListener listener) {
        statusList.remove(listener);

        if (statusList.size() == 0) {
            // there are no registered connection listeners
            Log.d(TAG, "[ " + listener.provideName() + "] Last listener was removed, un-subscribing from network Updates");
            unsubscribeFromNetworkUpdates();
        }

    }

    public void removeQualityListener(ConnectionClassManager.ConnectionClassStateChangeListener listener) {
        // TODO: 6/29/2016 find lifecycle hooks appropriate for removing listeners from interactor
        qualityList.remove(listener);
    }

    private void unsubscribeFromNetworkUpdates() {
        networkSubscription.unsubscribe();
    }

    /**
     * Subscribes the class to connectivity status changes
     */
    private void subscribeToNetworkChanges() {
        networkSubscription = new ReactiveNetwork()
                .observeNetworkConnectivity(context)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ConnectivityStatus>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ConnectivityStatus connectivityStatus) {
                        Log.d(TAG, "onNext() called with: connectivityStatus = [" + connectivityStatus + "]");
                        netStatus = connectivityStatus;
                        notifyStatusListeners(connectivityStatus);

                    }
                });
    }

    /**
     * @param lastKnownConnectivityStatus Notifies interested parties in the most recent
     *                                    ConnectivityStatus information
     */
    private void notifyStatusListeners(ConnectivityStatus lastKnownConnectivityStatus) {
        for (OnConnectionChangedListener listener : statusList) {
            listener.onConnectionChanged(lastKnownConnectivityStatus);
        }
    }

    private void notifyQualityListeners(ConnectionQuality connectionQuality){
        for (ConnectionClassManager.ConnectionClassStateChangeListener listener : qualityList) {
            listener.onBandwidthStateChange(connectionQuality);
        }
    }

    /**
     * @param listener class that implements the {@link OnConnectionChangedListener} interface
     * @return boolean representing if the tag value of the implementing class already exists in the listener list.
     */
    private boolean containsStatusListener(OnConnectionChangedListener listener) {
        boolean isListenerInList = false;

        for (OnConnectionChangedListener item : statusList) {
            if (item.provideName().equals(listener.provideName())) {
                isListenerInList = true;
            }
        }

        return isListenerInList;
    }


    @Override
    public void onBandwidthStateChange(ConnectionQuality bandwidthState) {
        Log.d(TAG, "onBandwidthStateChange() called with: bandwidthState = [" + bandwidthState + "]");
        netQuality = bandwidthState;
        notifyQualityListeners(bandwidthState);
    }

    public ConnectionQuality getNetQuality() {
        return netQuality;
    }
}