package com.ruthlessprogramming.flickrreferenceapp.management;

import com.github.pwittchen.reactivenetwork.library.ConnectivityStatus;

/**
 * Created by billy on 6/22/2016.
 * for FlickrReferenceApp
 */

public interface OnConnectionChangedListener {

    void onConnectionChanged(ConnectivityStatus connectivityStatus);

    void onCurrentConnectionStatus(ConnectivityStatus connectivityStatus);

    String provideName();

}
