package com.ruthlessprogramming.flickrreferenceapp.management;

import android.location.Location;

/**
 * Created by billy on 6/25/2016.
 * for FlickrReferenceApp
 */
public interface OnLocationChangedListener {

    void onLocationChanged(Location location);

    String provideName();

    void onLastKnownLocation(Location location);
}