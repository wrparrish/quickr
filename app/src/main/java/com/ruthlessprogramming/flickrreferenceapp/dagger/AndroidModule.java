package com.ruthlessprogramming.flickrreferenceapp.dagger;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ruthlessprogramming.flickrreferenceapp.interactor.PhotoInteractor;
import com.ruthlessprogramming.flickrreferenceapp.interactor.PhotoInteractorImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */
@Module
public class AndroidModule {

    private final Application application;

    public AndroidModule(final Application application) {
        this.application = application;
    }


    @Provides
    @Singleton
    Application providesApplication() {
        return application;
    }

    @Provides
    @Singleton
    Context providesApplicationContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences(final Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    SharedPreferences.Editor providesPreferenceEditor(final SharedPreferences preferences) {
        return preferences.edit();
    }

    @Provides
    @Singleton
    PhotoInteractor providesPhotoInteractor() {
        return new PhotoInteractorImpl();
    }

}