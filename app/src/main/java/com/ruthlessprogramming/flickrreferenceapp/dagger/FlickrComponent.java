package com.ruthlessprogramming.flickrreferenceapp.dagger;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */

import com.ruthlessprogramming.flickrreferenceapp.App;
import com.ruthlessprogramming.flickrreferenceapp.MainActivity;
import com.ruthlessprogramming.flickrreferenceapp.SplashActivity;
import com.ruthlessprogramming.flickrreferenceapp.interactor.PhotoInteractorImpl;
import com.ruthlessprogramming.flickrreferenceapp.mvp.presenter.MainActivityPresenterImpl;
import com.ruthlessprogramming.flickrreferenceapp.mvp.view.rec_view.FlickrPhotoAdapter;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {AndroidModule.class, NetModule.class, ManagerModule.class})
public interface FlickrComponent {
    void inject(App app);

    void inject(MainActivity mainActivity);

    void inject(MainActivityPresenterImpl mainActivityPresenter);

    void inject(PhotoInteractorImpl photoInteractor);

    void inject(FlickrPhotoAdapter flickrPhotoAdapter);

    void inject(SplashActivity splashActivity);

}
