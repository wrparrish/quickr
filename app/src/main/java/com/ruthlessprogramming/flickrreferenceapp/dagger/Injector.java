package com.ruthlessprogramming.flickrreferenceapp.dagger;

import com.ruthlessprogramming.flickrreferenceapp.App;
import com.ruthlessprogramming.flickrreferenceapp.MainActivity;
import com.ruthlessprogramming.flickrreferenceapp.SplashActivity;
import com.ruthlessprogramming.flickrreferenceapp.interactor.PhotoInteractorImpl;
import com.ruthlessprogramming.flickrreferenceapp.mvp.presenter.MainActivityPresenterImpl;
import com.ruthlessprogramming.flickrreferenceapp.mvp.view.rec_view.FlickrPhotoAdapter;


/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */

public class Injector {

    private static Injector instance;
    private FlickrComponent flickrComponent;


    private Injector(final FlickrComponent flickrComponent) {
        this.flickrComponent = flickrComponent;
    }

    public static Injector get(FlickrComponent flickrComponent) {
        if (instance == null) {
            instance = new Injector(flickrComponent);
        }

        return instance;
    }

    public void setComponent(FlickrComponent flickrComponent) {
        this.flickrComponent = flickrComponent;
    }

    public void inject(App app) {
        flickrComponent.inject(app);
    }

    public void inject(MainActivity mainActivity) {
        flickrComponent.inject(mainActivity);
    }

    public void inject(MainActivityPresenterImpl mainActivityPresenter) {
        flickrComponent.inject(mainActivityPresenter);
    }

    public void inject(PhotoInteractorImpl photoInteractor) {
        flickrComponent.inject(photoInteractor);
    }

    public void inject(FlickrPhotoAdapter flickrPhotoAdapter) {
        flickrComponent.inject(flickrPhotoAdapter);
    }

    public void inject(SplashActivity splashActivity) {
        flickrComponent.inject(splashActivity);
    }
}
