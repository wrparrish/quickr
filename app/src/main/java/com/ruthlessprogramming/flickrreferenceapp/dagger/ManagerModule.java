package com.ruthlessprogramming.flickrreferenceapp.dagger;

import android.content.Context;

import com.ruthlessprogramming.flickrreferenceapp.management.LocationManager;
import com.ruthlessprogramming.flickrreferenceapp.management.NetworkManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by billy on 6/22/2016.
 * for FlickrReferenceApp
 */

@Module
public class ManagerModule {

    @Provides
    @Singleton
    public NetworkManager providesNetworkManager(Context context) {
        return new NetworkManager(context);
    }

    @Provides
    @Singleton
    public LocationManager providesLocationManager(Context context) {
        return new LocationManager(context);
    }
}
