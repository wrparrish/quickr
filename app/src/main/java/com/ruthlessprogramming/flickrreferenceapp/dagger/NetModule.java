package com.ruthlessprogramming.flickrreferenceapp.dagger;

import android.app.Application;

import com.facebook.stetho.okhttp.StethoInterceptor;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ruthlessprogramming.flickrreferenceapp.net.FlickrApi;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */
@Module
public class NetModule {
    private final String baseUrl = "https://api.flickr.com/services/";
    private final String apiKey = "0212a76146f4d48783a5d1a6c82e173a";


    @Provides
    @Singleton
    Cache providesOkHttpCache(final Application application) {
        final int cacheSize = 12 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cacheSize);

    }

    @Provides
    @Singleton
    Gson providesGson() {
        final GsonBuilder gsonBuilder = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);

        return gsonBuilder.create();
    }


    @Provides
    @Singleton
    OkHttpClient providesStethoOkHttpClient(final Cache cache) {
        final OkHttpClient client = new OkHttpClient();
        client.networkInterceptors().add(new StethoInterceptor());
        client.networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = HttpUrl.get(original.url());

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("api_key", apiKey)
                        .addQueryParameter("format", "json")
                        .addQueryParameter("nojsoncallback", "1")
                        .addQueryParameter("extras", "url_m")
                        .build();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        client.setCache(cache);
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(10, TimeUnit.SECONDS);
        return client;
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(final Gson gson, final OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    FlickrApi providesFlickrApi(Retrofit retrofit) {
        return retrofit.create(FlickrApi.class);
    }


    @Provides
    @Singleton
    Picasso providesPicasso(final Application application) {
        return Picasso.with(application);
    }

}