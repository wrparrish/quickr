package com.ruthlessprogramming.flickrreferenceapp.interactor;


import com.ruthlessprogramming.flickrreferenceapp.mvp.model.PhotoResponse;

import rx.Observable;

/**
 * Created by billy on 6/20/2016.
 * for Quickr.
 */
public interface PhotoInteractor {

    Observable<PhotoResponse> loadPhotosByQuery(String queryText);

    Observable<PhotoResponse> loadRecent();

    Observable<PhotoResponse> loadNearbyPhotos();
}
