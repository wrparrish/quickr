package com.ruthlessprogramming.flickrreferenceapp.interactor;

import android.util.Log;

import com.facebook.network.connectionclass.ConnectionClassManager;
import com.facebook.network.connectionclass.ConnectionQuality;
import com.ruthlessprogramming.flickrreferenceapp.App;

import com.ruthlessprogramming.flickrreferenceapp.management.LocationManager;

import com.ruthlessprogramming.flickrreferenceapp.management.NetworkManager;

import com.ruthlessprogramming.flickrreferenceapp.mvp.model.PhotoResponse;
import com.ruthlessprogramming.flickrreferenceapp.net.FlickrApi;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by billy on 6/20/2016.
 * for Quickr.
 */
public class PhotoInteractorImpl implements PhotoInteractor, ConnectionClassManager.ConnectionClassStateChangeListener {
    private static final String TAG = "PhotoInteractorImpl";
    private static final String RECENT = "flickr.photos.getRecent";
    private static final String SEARCH = "flickr.photos.search";



    @Inject
    FlickrApi api;

    @Inject
    LocationManager locationManager;

    @Inject
    NetworkManager networkManager;


    public PhotoInteractorImpl() {
        App.injector.inject(this);
        networkManager.addQualityListener(this);

    }

    @Override
    public Observable<PhotoResponse> loadPhotosByQuery(String queryText) {
        Log.d(TAG, "loadPhotosByQuery: ");
        Log.d(TAG, "currentConnectionQuality: [" + ConnectionClassManager.getInstance().getCurrentBandwidthQuality() + "]");
        return api.getPhotos(SEARCH, queryText)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<PhotoResponse> loadRecent() {
        Log.d(TAG, "loadRecent() called");
        return api.getRecentPhotos(RECENT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public Observable<PhotoResponse> loadNearbyPhotos() {
        Log.d(TAG, "loadNearbyPhotos: lat [" + locationManager.getGpsLat() + "]  lon [" + locationManager.getGpsLong() + "]");
        return api.getNearbyPhotos(SEARCH, true, locationManager.getGpsLat(), locationManager.getGpsLong(), 15) // hard coded radius set to 15km
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    public void onBandwidthStateChange(ConnectionQuality bandwidthState) {
        Log.d(TAG, "onBandwidthStateChange() called with: bandwidthState = [" + bandwidthState + "]");
        //TODO  change quality / size of requested images, move extras out of net module and make them dynamic.


    }
}
