package com.ruthlessprogramming.flickrreferenceapp.mvp.presenter;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.ruthlessprogramming.flickrreferenceapp.mvp.model.PhotoResponse;
import com.ruthlessprogramming.flickrreferenceapp.mvp.view.MainActivityView;


/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */
public interface MainActivityPresenter extends MvpPresenter<MainActivityView> {

    void retrievePhotos(String queryText);

    void loadStarterPhotos();

    void processResponseData(PhotoResponse response);
}
