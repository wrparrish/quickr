package com.ruthlessprogramming.flickrreferenceapp.mvp.presenter;

import android.util.Log;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.ruthlessprogramming.flickrreferenceapp.App;
import com.ruthlessprogramming.flickrreferenceapp.interactor.PhotoInteractor;
import com.ruthlessprogramming.flickrreferenceapp.management.LocationManager;
import com.ruthlessprogramming.flickrreferenceapp.management.NetworkManager;
import com.ruthlessprogramming.flickrreferenceapp.mvp.model.Photo;
import com.ruthlessprogramming.flickrreferenceapp.mvp.model.PhotoResponse;
import com.ruthlessprogramming.flickrreferenceapp.mvp.view.MainActivityView;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */
public class MainActivityPresenterImpl extends MvpBasePresenter<MainActivityView> implements MainActivityPresenter {
    private static final String TAG = "MainActivityPresenter";
    private final String genericErrorMessage = "Sorry, unable to load  the photos";
    private Subscription recentSubscription;
    private Subscription querySubscription;
    private String query;

    @Inject
    PhotoInteractor interactor;

    @Inject
    LocationManager locationManager;

    @Inject
    NetworkManager networkManager;



    public MainActivityPresenterImpl() {
        App.injector.inject(this);
    }


    @Override
    public void retrievePhotos(String queryText) {
        Log.d(TAG, "retrievePhotos() called with: queryText = [" + queryText + "]");
        getView().showLoading();
        query = queryText;
        querySubscription = interactor.loadPhotosByQuery(queryText)
                .subscribe(new Subscriber<PhotoResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted() called");
                        querySubscription.unsubscribe();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: [" + e.toString() + "]");
                        if (isViewPresent()) {
                            getView().showError(genericErrorMessage);
                        }
                    }

                    @Override
                    public void onNext(PhotoResponse photoResponse) {
                        Log.d(TAG, "onNext() called with: photoResponse = [" + photoResponse + "]");
                        if (isResponseValid(photoResponse)) {
                            processResponseData(photoResponse);
                        } else {
                            if (isViewPresent()) {
                                getView().showError(genericErrorMessage);
                            }
                        }
                    }
                });
    }

    @Override
    public void loadStarterPhotos() {
        getView().showLoading();
        Log.d(TAG, "loadStarterPhotos() called");

        if (locationManager.getCurrentGpsLocation() != null) {
            getPhotosWithLocation();

        } else {
            getPhotos();
        }

    }

    private void getPhotos() {
        recentSubscription = interactor.loadRecent()
                .subscribe(new Subscriber<PhotoResponse>() {
                    @Override
                    public void onCompleted() {
                        recentSubscription.unsubscribe();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (isViewPresent()) {
                            getView().showError(e.toString());
                        }
                    }

                    @Override
                    public void onNext(PhotoResponse photoResponse) {
                        if (isResponseValid(photoResponse)) {

                            processResponseData(photoResponse);
                        } else {
                            if (isViewPresent()) {
                                getView().showError(genericErrorMessage);
                            }
                        }

                    }
                });

    }

    private void getPhotosWithLocation() {
        recentSubscription = interactor.loadNearbyPhotos()
                .subscribe(new Subscriber<PhotoResponse>() {
                    @Override
                    public void onCompleted() {
                        recentSubscription.unsubscribe();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (isViewPresent()) {
                            getView().showError(e.toString());
                        }
                    }

                    @Override
                    public void onNext(PhotoResponse photoResponse) {
                        if (isResponseValid(photoResponse)) {

                            processResponseData(photoResponse);
                        } else {
                            if (isViewPresent()) {
                                getView().showError(genericErrorMessage);
                            }
                        }

                    }
                });
    }

    @Override
    public void processResponseData(PhotoResponse response) {
        Log.d(TAG, "processResponseData() called");
        if (!isViewPresent()) {
            return;
        }

        ArrayList<Photo> photoArrayList = response.getPhotos().getPhoto();

        if (photoArrayList != null && photoArrayList.size() > 0) {
            getView().showContent(photoArrayList);
        } else {
            getView().showError("Unable to retrieve photos related to [" + query + "]");
        }


    }

    private boolean isResponseValid(PhotoResponse photoResponse) {
        return (photoResponse != null && photoResponse.getPhotos() != null);
    }

    private boolean isViewPresent() {
        return (getView() != null && isViewAttached());
    }


}
