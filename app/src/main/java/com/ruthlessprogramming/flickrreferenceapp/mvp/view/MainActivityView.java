package com.ruthlessprogramming.flickrreferenceapp.mvp.view;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.ruthlessprogramming.flickrreferenceapp.mvp.model.Photo;

import java.util.ArrayList;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */
public interface MainActivityView extends MvpView {

    void showLoading();

    void hideLoading();

    void showContent(ArrayList<Photo> photoArrayList);

    void showError(String message);

    void hideError();

    void showOffline();

    void hideOffline();

}
