package com.ruthlessprogramming.flickrreferenceapp.mvp.view.rec_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.facebook.network.connectionclass.DeviceBandwidthSampler;
import com.ruthlessprogramming.flickrreferenceapp.R;
import com.ruthlessprogramming.flickrreferenceapp.mvp.model.Photo;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */

public class FlickrPhotoAdapter extends RecyclerView.Adapter<FlickrViewHolder> {
    private final Context context;
    private ArrayList<Photo> photos = new ArrayList<>();
    private DeviceBandwidthSampler bandwidthSampler = DeviceBandwidthSampler.getInstance();
    private Picasso picasso;


    public FlickrPhotoAdapter(Context context) {
        this.context = context;
        picasso = Picasso.with(context);
        picasso.setIndicatorsEnabled(true);

    }

    @Override
    public FlickrViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FlickrViewHolder(LayoutInflater.from(context).inflate(R.layout.row_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(FlickrViewHolder holder, int position) {
        Photo photo = photos.get(position);
        bandwidthSampler.startSampling();
        picasso
                .load(photo.getUrlM())
                .placeholder(R.drawable.placeholder)
                .fit()
                .centerCrop()
                .into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        bandwidthSampler.stopSampling();
                    }

                    @Override
                    public void onError() {
                        bandwidthSampler.stopSampling();
                    }
                });

    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }
}
