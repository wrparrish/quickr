package com.ruthlessprogramming.flickrreferenceapp.mvp.view.rec_view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.ruthlessprogramming.flickrreferenceapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */

class FlickrViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.iv_flickr_photo)
    ImageView imageView;


    FlickrViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
