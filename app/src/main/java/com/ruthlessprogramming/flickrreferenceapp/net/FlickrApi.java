package com.ruthlessprogramming.flickrreferenceapp.net;


import com.ruthlessprogramming.flickrreferenceapp.mvp.model.PhotoResponse;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */

public interface FlickrApi {

    @GET("rest")
    Observable<PhotoResponse> getPhotos(@Query("method") String method, @Query("text") String queryText);

    @GET("rest")
    Observable<PhotoResponse> getRecentPhotos(@Query("method") String method);

    @GET("rest")
    Observable<PhotoResponse> getNearbyPhotos(@Query("method") String method, @Query("has_geo") boolean hasGeo,  @Query("lat") double latitude ,  @Query("lon") double longitude, @Query("radius") int radius);

}
