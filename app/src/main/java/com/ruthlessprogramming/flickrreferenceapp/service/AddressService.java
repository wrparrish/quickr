package com.ruthlessprogramming.flickrreferenceapp.service;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import java.io.IOException;
import java.util.List;

/**
 * Created by billy on 6/25/2016.
 * for FlickrReferenceApp
 */
public class AddressService extends IntentService {
    private static final String TAG = "AddressService";
    private ResultReceiver receiver;
    private double lat;
    private double lng;

    public AddressService() {
        super("AddressIntentService");
        setIntentRedelivery(true);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        receiver = bundle.getParcelable("receiver");
        lat = bundle.getDouble("lat", -1);
        lng = bundle.getDouble("lng", -1);
        getAddress();

    }

    private void getAddress() {


        Geocoder geocoder = new Geocoder(this);
        if (Geocoder.isPresent()) {

            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocation(lat, lng, 1);
            } catch (IOException e) {
                Log.e(TAG, "getUserDefaultLocationLatLng: " + e.getMessage(), e);
            }

            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);

                String addr = address.getAddressLine(0);
                String city = address.getAddressLine(1);
                String country = address.getAddressLine(2);

                // handle null values returned by string
                if (addr == null || addr.equals("null")) {
                    addr = "";
                }

                if (city == null || city.equals("null")) {
                    city = "";
                }

                if (country == null || country.equals("null")) {
                    country = "";
                }


                String toReturn = String.format("%s, %s, %s", addr, city, country);

                Bundle bundle = new Bundle();
                bundle.putString("address", toReturn);
                receiver.send(0, bundle);

            }

        } else {
            Log.e(TAG, "getAddress: GEOCODER not present");
        }

    }
}