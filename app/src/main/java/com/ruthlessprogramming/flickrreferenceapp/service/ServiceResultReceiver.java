package com.ruthlessprogramming.flickrreferenceapp.service;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by billy on 6/25/2016.
 * for FlickrReferenceApp
 */
@SuppressLint("ParcelCreator")
public class ServiceResultReceiver extends android.support.v4.os.ResultReceiver {

    private Receiver receiver;

    public ServiceResultReceiver(Handler handler) {
        super(handler);
        // TODO Auto-generated constructor stub
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (receiver != null) {
            receiver.onReceiveResult(resultCode, resultData);
        }
    }

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);

    }

}