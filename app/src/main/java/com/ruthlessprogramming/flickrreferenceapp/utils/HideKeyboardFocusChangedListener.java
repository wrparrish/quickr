package com.ruthlessprogramming.flickrreferenceapp.utils;

import android.content.Context;
import android.view.View;

/**
 * Created by billy on 6/20/2016.
 * for FlickrReferenceApp
 */

class HideKeyboardFocusChangedListener implements View.OnFocusChangeListener {

    private final Context context;

    public HideKeyboardFocusChangedListener(final Context context) {
        this.context = context;
    }

    /**
     * Called when the focus state of a view has changed.
     *
     * @param v        The view whose state has changed.
     * @param hasFocus The new focus state of v.
     */
    @Override
    public void onFocusChange(final View v, final boolean hasFocus) {
        if (!hasFocus) {
            Utils.hideKeyboard(context, v);
        }
    }
}
