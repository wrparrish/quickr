package com.ruthlessprogramming.flickrreferenceapp.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by billy on 6/20/2016.
 * for Quickr.
 */

public class Utils {

    public static void hideKeyboard(final Context context, final View view) {
        final InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
